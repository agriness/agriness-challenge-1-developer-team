# Desafio 1 - Agriness - Desenvolvimento

## Introdução

Bem-vindo ao Desafio Agriness. 

Esse desafio tem apenas o objetivo de entender como você aplicaria alguns conceitos técnicos que atualmente são utilizados por nós.

A seguir descrevemos alguns requisitos de um problema que você poderá resolver utilizando as tecnologias de sua escolha. Entretanto, sugerimos a utilização de algum(as) das tecnologias de nossa stack (Python, Django, Flask, PostgreSQL, ElasticSearch, Django Rest Framework).

## Problema

Desejos administrar as operações dentro de um Centro de Produção. Neste centro são administrados matérias-prima, funcionários e produtos finais, sendo essenciais alguns relatórios com filtros específicos.

Nosso trabalho é construir uma API que nos forneça as funcionalidades exigidas pelo cliente.

### Entidades

Temos três entidades que precisam ser administradas por nossa API. As operações que poderão ser realizadas nas entidades são: adição, edição e remoção.


    Entidade 1: Matéria-Prima
        - Nome: String (limite de 100 caracteres)
        - Quantidade em Estoque: Inteiro

    Entidade 2: Funcionário
        - Nome: String (limite de 100 caracteres)
        - Jornada de trabalho (Opções 4h, 6h, 8h)
    
    Entidade 3: Produto Final
        - Nome: String (limite de 100 caracteres)
        - Matérias-primas utilizadas (referência para Matéria-prima)
        - Funcionário responsável (referência para Funcionário)

### Relatórios

E exigência do cliente que os seguintes relatórios sejam gerados:

1. Estoque de Matéria-Prima: listagem com quantidade de cada matéria-prima disponível no estoque.
    - Filtro específico: o usuário pode selecionar apenas matérias cuja quantidade seja inferior a um determinado valor.

2. Lista de Produtos Finais:
    - Filtro específico 1: o usuário pode pesquisar pelo nome do funcionário responsável.
    - Filtro específico 2: o usuário pode filtrar pela matéria-prima usada. 

Exemplos:

Relatório de Estoque de matéria-prima sem filtro:

    1 - Fermento (Tipo 1) - Quantidade: 10
    2 - Açúcar - Quantidade: 04
    3 - Mantega - Quantidade: 06
    4 - Fermento (Tipo 2) - Quantidade: 02

Relatório de Estoque de máteria-prima filtrado por quantidade menor que 5

    2 - Açúcar - Quantidade: 04
    4 - Fermento (Tipo 2) - Quantidade: 02

Relatório de Lista de Produtos Finais sem filtro

    1 - Macarrão Instânteneo
        - Matérias-primas: Fermento (Tipo 1); Mantega.
        - Funcionário: Maria Joaquina      

    2 - Macarrão Integral
        - Matérias-primas: Fermento (Tipo 2).
        - Funcionário: João Marcos.

Relatório de Lista de Produtos Finais com pesquisa de funcionário com nome "João".

    2 - Macarrão Integral
        - Matérias-primas: Fermento (Tipo 2).
        - Funcionário: João Marcos.    


Relatório de Lista de Produtos Finais com filtro de máteria-prima "Fermento (Tipo 1)".

    1 - Macarrão Instânteneo
        - Matérias-primas: Fermento (Tipo 1); Mantega.
        - Funcionário: Maria Joaquina   


## Observações:

- Documente a API para que o cliente possa utilizá-la diretamente.
- Implemente testes para garantir a qualidade da entrega.
- Documente o projeto para que outros desenvolvedores possam executá-lo facilmente.
- Ao concluir compartilhe conosco o repositório GIT (Bitbucket ou GitHub). Certifique-se que teremos acesso total a ele. 
- Não se preocupe autenticação ou segurança nesse momento.